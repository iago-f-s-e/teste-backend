# Desafio NodeJS - Ioasys

## Objetivo
Desenvolver uma API  que o site IMDb irá consultar para exibir seu conteúdo.


## Tecnologias
As seguintes tecnologias foram usadas para construir este projeto

  - [NodeJS](https://nodejs.org/en/)
  - [Typescript](https://www.typescriptlang.org/)
  - [Express](https://expressjs.com/pt-br/)
  - [OvernightJS](https://github.com/seanpmaxwell/overnight/#readme)
  - [PostgreSQL](https://www.postgresql.org/)
  - [TypeORM](https://typeorm.io/)
  - [Helmet](https://helmetjs.github.io/)
  - [Babel](https://babeljs.io/)
  - [Jest](https://jestjs.io/pt-BR/)
  - [Supertest](https://github.com/visionmedia/supertest#readme)

## Como executar o projeto

### Pré-requisitos
  - NodeJS
  - PostgreSQL
  - Yarn ou NPM
  - Git
  - Postman

### Passo 1: Clonar o repositório
```
$ git clone https://iago-f-s-e@bitbucket.org/iago-f-s-e/teste-backend.git
```

```
$ cd teste-backend
```

### Passo 2: Configurar as variaveis de ambiente
  - copie o conteúdo do arquivo ***.env.example***
  - crie um arquivo ***.env*** e cole o conteúdo
  - crie um banco de dados
  - atribua seu usuario root, password e o nome do banco nas variaveis ***TYPEORM_USERNAME***, ***TYPEORM_PASSWORD*** e ***TYPEORM_DATABASE***
  
### Passo 3: Instalar as dependencias
```
yarn
```
ou
```
npm install
```

### Passo 4: Rodar as migrations
```
yarn typeorm:run
```

ou

```
npm run typeorm:run

```

### Passo 5: Rodar o projeto

```
yarn start
```

ou

```
npm run start

```

### Passo 6: Postman
Import o arquivo ***postman.json*** no seu ***Postman*** para poder ter acesso a documentação

### Opcional: Rodar os testes
```
yarn test
```

ou

```
npm run test

```

###### Caso ocorra algum problema, estou a disposição para auxiliar:
  - wpp: (75) 9 9950-3472
  - email: iago.fagundes@outlook.com

## Decisões

### Framework 
  foi recomendado utilizar o **ExpressJS** ou **SailsJS**, porém, utilizei o **OvernightJS** porque utiliza o express e design pattern decorator e com isso a estrutura de mapear as rotas fica mais limpa

### Arquitetura
  Decidi utilizar minha arquitetura favorita (no momento), porém, o uso dessa arquitetura fez com o que o projeto ganhasse uma complexidade desnecessária. Me inspirei no SOLID e no DDD para desenvolver essa arquitetura

### Branch
  Decidi trocar a branch principal para se adequar aos padrões do github.

## Justificativas
Por conta do tempo não consegui adicionar:
  - testes: testes unitários e end2end na camada de ***auth***
  - banco de dados: criar a tabela de imagens e propriedades nas tabelas de category e actor
  - transformer do typeorm para encriptar o email
  - documentação utilizando swagger
  - hospedar no heroku
  - hospedar no S3
