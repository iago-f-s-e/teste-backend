import { ChildControllers, Controller } from '@overnightjs/core'
import { MovieController } from '@domain/controllers'
import { UserController } from '@auth/controllers'

@Controller(`api/${process.env.API_VERSION}`)
@ChildControllers([
  new MovieController(),
  new UserController()
])
export class MappingControllers {}
