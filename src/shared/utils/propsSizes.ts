export function propsSizes (prop: string, size: number): void {
  if (prop.length > size) throw new Error('Value too long')
}
