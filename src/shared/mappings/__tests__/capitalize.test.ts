import { capitalize } from '@src/shared'

describe('Capitalize', () => {
  it('must capitalize the payload', () => {
    const lorem = 'LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY.'
    const loremCapitalized = 'Lorem Ipsum Is Simply Dummy Text Of The Printing And Typesetting Industry.'

    const capitalized = capitalize(lorem)

    expect(capitalized).toBe(loremCapitalized)
  })
})
