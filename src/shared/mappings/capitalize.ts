export function capitalize (payload: string): string {
  return payload
    .split(' ')
    .map(piece => {
      const lowerCase = piece.slice(1).toLowerCase()

      return `${piece[0]}${lowerCase}`
    })
    .join(' ')
}
