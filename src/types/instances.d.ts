import { MovieServices } from '@domain/services'
import { UserServices } from '@auth/services'

export interface Instances {
  movie: MovieServices
  user: UserServices
}
