import { typeormHandlers } from '@src/infra'
import { Instances } from '@src/types/instances'

import { Movie } from '@domain/entities'
import { MovieServices } from '@domain/services'

import { User } from '@auth/entities'
import { UserServices } from '@auth/services'

export function initializeInstances (): Instances {
  return {
    movie: new MovieServices(typeormHandlers(Movie)),
    user: new UserServices(typeormHandlers(User))
  }
}
