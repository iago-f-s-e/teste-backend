import { capitalize } from '@src/shared'

interface TransformerCapitalize {
  from: (value: string) => string
  to: (value: string) => string
}

export function transformerCapitalize (): TransformerCapitalize {
  return {
    from: (value: string) => capitalize(value),
    to: (value: string) => value.toUpperCase()
  }
}
