import { v4 as generateUUID, validate as validateUUID } from 'uuid'

interface UUIDProtocols {
  generate: () => string
  validate: (id: string) => void
}

export function UUID (): UUIDProtocols {
  return {
    generate,
    validate
  }
}

function generate (): string {
  return generateUUID()
}

function validate (id: string):void {
  if (!validateUUID(id)) throw new Error('Invalid data')
}
