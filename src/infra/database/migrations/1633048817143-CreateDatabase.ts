import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateDatabase1633048817143 implements MigrationInterface {
  name = 'CreateDatabase1633048817143'

  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('CREATE TABLE "user" ("user_id" uuid NOT NULL, "name" character varying(100) NOT NULL, "username" character varying(20) NOT NULL, "email" character varying(100) NOT NULL, "password" character varying NOT NULL, "is_active" boolean NOT NULL DEFAULT true, "is_admin" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_758b8ce7c18b9d347461b30228d" PRIMARY KEY ("user_id"))')
    await queryRunner.query('CREATE TABLE "category" ("category_id" character varying NOT NULL, "name" character varying(100) NOT NULL, CONSTRAINT "PK_cc7f32b7ab33c70b9e715afae84" PRIMARY KEY ("category_id"))')
    await queryRunner.query('CREATE TABLE "movie" ("movie_id" uuid NOT NULL, "category_id" character varying NOT NULL, "title" character varying(100) NOT NULL, "description" character varying(1000) NOT NULL, "director" character varying(100) NOT NULL, "rating" double precision NOT NULL DEFAULT \'0\', "rating_amount" double precision NOT NULL DEFAULT \'0\', "release" TIMESTAMP NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT \'now()\', "update_at" TIMESTAMP NOT NULL DEFAULT \'now()\', CONSTRAINT "PK_f38244c6e76d8e50e1a590f6744" PRIMARY KEY ("movie_id"))')
    await queryRunner.query('CREATE TABLE "actor" ("actor_id" uuid NOT NULL, "name" character varying(100) NOT NULL, CONSTRAINT "PK_4faabcb28eb548caa2404261e4f" PRIMARY KEY ("actor_id"))')
    await queryRunner.query('CREATE TABLE "movie_actors_actor" ("movieMovieId" uuid NOT NULL, "actorActorId" uuid NOT NULL, CONSTRAINT "PK_b74c2416670f1a563ae8a386b30" PRIMARY KEY ("movieMovieId", "actorActorId"))')
    await queryRunner.query('CREATE INDEX "IDX_c97ad33f2ad62f033c9b37a7ca" ON "movie_actors_actor" ("movieMovieId") ')
    await queryRunner.query('CREATE INDEX "IDX_ba1a377fe3ea8b9424e312489e" ON "movie_actors_actor" ("actorActorId") ')
    await queryRunner.query('ALTER TABLE "movie" ADD CONSTRAINT "FK_8cc157746ce57bc44cf9e356fbd" FOREIGN KEY ("category_id") REFERENCES "category"("category_id") ON DELETE RESTRICT ON UPDATE CASCADE')
    await queryRunner.query('ALTER TABLE "movie_actors_actor" ADD CONSTRAINT "FK_c97ad33f2ad62f033c9b37a7ca9" FOREIGN KEY ("movieMovieId") REFERENCES "movie"("movie_id") ON DELETE RESTRICT ON UPDATE CASCADE')
    await queryRunner.query('ALTER TABLE "movie_actors_actor" ADD CONSTRAINT "FK_ba1a377fe3ea8b9424e312489e2" FOREIGN KEY ("actorActorId") REFERENCES "actor"("actor_id") ON DELETE NO ACTION ON UPDATE NO ACTION')
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER TABLE "movie_actors_actor" DROP CONSTRAINT "FK_ba1a377fe3ea8b9424e312489e2"')
    await queryRunner.query('ALTER TABLE "movie_actors_actor" DROP CONSTRAINT "FK_c97ad33f2ad62f033c9b37a7ca9"')
    await queryRunner.query('ALTER TABLE "movie" DROP CONSTRAINT "FK_8cc157746ce57bc44cf9e356fbd"')
    await queryRunner.query('DROP INDEX "IDX_ba1a377fe3ea8b9424e312489e"')
    await queryRunner.query('DROP INDEX "IDX_c97ad33f2ad62f033c9b37a7ca"')
    await queryRunner.query('DROP TABLE "movie_actors_actor"')
    await queryRunner.query('DROP TABLE "actor"')
    await queryRunner.query('DROP TABLE "movie"')
    await queryRunner.query('DROP TABLE "category"')
    await queryRunner.query('DROP TABLE "user"')
  }
}
