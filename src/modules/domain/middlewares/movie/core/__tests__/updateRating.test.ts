
import { RequestToUpdateMovie } from '@src/modules/domain/controllers'
import { NextFunction, Request } from 'express'
import { updateRating } from '../updateRating'

const mockedNext: NextFunction = jest.fn()
let response: () => any

describe('Create Movie', () => {
  beforeEach(() => {
    response = () => {
      const mockResponse: any = {
        json: jest.fn((payload: any) => {
          return {
            ...mockResponse,
            body: payload
          }
        }),
        status: jest.fn((code: number) => {
          return {
            ...mockResponse,
            statusCode: code
          }
        })
      }

      return mockResponse
    }
  })

  it('must be possible to valide the movie request', () => {
    const props: RequestToUpdateMovie = {
      rating: 1
    }

    const mockedRequest = {
      body: props
    } as Request

    const mockedResponse = response()

    updateRating(mockedRequest, mockedResponse, mockedNext)

    expect(mockedNext).toBeCalled()
  })

  it('throw new error if invalid number format', () => {
    const mockedRequest = {
      body: {
        rating: 'one'
      }
    } as Request

    const mockedResponse = response()

    updateRating(mockedRequest, mockedResponse, mockedNext)

    expect(mockedResponse.status).toBeCalledWith(400)
    expect(mockedResponse.json).toBeCalledWith({ error: 'Number with invalid format' })
  })

  it('throw new error if invalid rating', () => {
    const mockedRequest = {
      body: {
        rating: -1
      }
    } as Request

    const mockedResponse = response()

    updateRating(mockedRequest, mockedResponse, mockedNext)

    expect(mockedResponse.status).toBeCalledWith(400)
    expect(mockedResponse.json).toBeCalledWith({ error: 'Invalid rating' })
  })
})
