import {
  SIZE_ACTOR_NAME,
  SIZE_CATEGORY_NAME,
  SIZE_MOVIE_DESCRIPTION,
  SIZE_MOVIE_DIRECTOR,
  SIZE_MOVIE_TITLE
} from '@domain/constants'

import { RequestToCreateMovie } from '@domain/controllers'

import { generateLongProps } from '@test/util/generateLongProps'
import { NextFunction, Request } from 'express'
import { createMovie } from '../createMovie'

const mockedNext: NextFunction = jest.fn()
let response: () => any

describe('Create Movie', () => {
  beforeEach(() => {
    response = () => {
      const mockResponse: any = {
        json: jest.fn((payload: any) => {
          return {
            ...mockResponse,
            body: payload
          }
        }),
        status: jest.fn((code: number) => {
          return {
            ...mockResponse,
            statusCode: code
          }
        })
      }

      return mockResponse
    }
  })

  it('must be possible to valide the movie request', () => {
    const props: RequestToCreateMovie = {
      actors: [{
        name: 'Donald Glover'
      }],
      category: {
        name: 'Romance'
      },
      description: 'Something about movie',
      director: 'any director',
      release: '29/09/2021',
      title: 'any title'
    }

    const mockedRequest = {
      body: props
    } as Request

    const mockedResponse = response()

    createMovie(mockedRequest, mockedResponse, mockedNext)

    expect(mockedNext).toBeCalled()
  })

  it('throw new error if missing required values', () => {
    const mockedRequest = {
      body: {}
    } as Request

    const mockedResponse = response()

    createMovie(mockedRequest, mockedResponse, mockedNext)

    expect(mockedResponse.status).toBeCalledWith(400)
    expect(mockedResponse.json).toBeCalledWith({ error: 'Missing required values' })
  })

  it('throw new error if empty required values', () => {
    const mockedRequest = {
      body: {
        actors: [],
        category: {
          name: 'Romance'
        },
        description: ' ',
        director: ' ',
        release: ' ',
        title: ' '
      }
    } as Request

    const mockedResponse = response()

    createMovie(mockedRequest, mockedResponse, mockedNext)

    expect(mockedResponse.status).toBeCalledWith(400)
    expect(mockedResponse.json).toBeCalledWith({ error: 'Missing required values' })
  })

  it('throw new error if missing or empty actor name', () => {
    const mockedRequestMissing = {
      body: {
        actors: [{ }],
        category: {
          name: 'Romance'
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      }
    } as Request

    const mockedRequestEmpty = {
      body: {
        actors: [{
          name: ' '
        }],
        category: {
          name: 'Romance'
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      }
    } as Request

    const mockedResponseEmpty = response()
    const mockedResponseMissing = response()

    createMovie(mockedRequestEmpty, mockedResponseEmpty, mockedNext)
    createMovie(mockedRequestMissing, mockedResponseMissing, mockedNext)

    expect(mockedResponseEmpty.status).toBeCalledWith(400)
    expect(mockedResponseMissing.status).toBeCalledWith(400)
    expect(mockedResponseEmpty.json).toBeCalledWith({ error: 'Missing required values' })
    expect(mockedResponseMissing.json).toBeCalledWith({ error: 'Missing required values' })
  })

  it('throw new error if missing or empty category name', () => {
    const mockedRequestMissing = {
      body: {
        actors: [{
          name: 'Donald Glover'
        }],
        category: {},
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      }
    } as Request

    const mockedRequestEmpty = {
      body: {
        actors: [{
          name: 'Donald Glover'
        }],
        category: {
          name: ' '
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      }
    } as Request

    const mockedResponseEmpty = response()
    const mockedResponseMissing = response()

    createMovie(mockedRequestEmpty, mockedResponseEmpty, mockedNext)
    createMovie(mockedRequestMissing, mockedResponseMissing, mockedNext)

    expect(mockedResponseEmpty.status).toBeCalledWith(400)
    expect(mockedResponseMissing.status).toBeCalledWith(400)
    expect(mockedResponseEmpty.json).toBeCalledWith({ error: 'Missing required values' })
    expect(mockedResponseMissing.json).toBeCalledWith({ error: 'Missing required values' })
  })

  it('throw new error if too long values', () => {
    const mockedRequest = {
      body: {
        actors: [{
          name: generateLongProps(SIZE_ACTOR_NAME + 1)
        }],
        category: {
          name: generateLongProps(SIZE_CATEGORY_NAME + 1)
        },
        description: generateLongProps(SIZE_MOVIE_DESCRIPTION + 1),
        director: generateLongProps(SIZE_MOVIE_DIRECTOR + 1),
        release: '29/09/2021',
        title: generateLongProps(SIZE_MOVIE_TITLE + 1)
      }
    } as Request

    const mockedResponse = response()

    createMovie(mockedRequest, mockedResponse, mockedNext)

    expect(mockedResponse.status).toBeCalledWith(400)
    expect(mockedResponse.json).toBeCalledWith({ error: 'Value too long' })
  })
})
