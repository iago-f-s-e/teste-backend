import { movieErrors } from '@src/modules/domain/errors'
import { NextFunction, Request, Response } from 'express'
import { checkProps } from '../functions'

export function createMovie (request: Request, response: Response, next: NextFunction): void {
  try {
    const props = checkProps(request.body)

    request.body = { props }

    next()
  } catch (error: any) {
    const { code, message } = movieErrors(error.message)

    response.status(code).json({ error: message })
  }
}
