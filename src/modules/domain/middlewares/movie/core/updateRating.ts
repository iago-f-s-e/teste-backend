import { movieErrors } from '@domain/errors'
import { NextFunction, Request, Response } from 'express'
import { checkUpdateRating } from '../functions'

export function updateRating (request: Request, response: Response, next: NextFunction): void {
  try {
    const props = checkUpdateRating(request.body)

    request.body = { props }

    next()
  } catch (error: any) {
    const { code, message } = movieErrors(error.message)

    response.status(code).json({ error: message })
  }
}
