import { UpdateMovieProps } from '@domain/services'
import { RequestToUpdateMovie } from '@domain/controllers'

export function checkUpdateRating (props: RequestToUpdateMovie): UpdateMovieProps {
  numberFormat(props)

  rating(props)

  return props
}

function numberFormat ({ rating }: Partial<RequestToUpdateMovie>): void {
  if (!rating || Number.isNaN(Number(rating))) throw new Error('Number with invalid format')
}

function rating ({ rating }: RequestToUpdateMovie): void {
  if (rating < 0 || rating > 4) throw new Error('Invalid rating')
}
