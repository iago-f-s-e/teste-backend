import { CreateMovieProps } from '@domain/services'
import { RequestToCreateMovie } from '@domain/controllers'
import { FormatDate } from '@domain/helpers'
import { propsSizes } from '@src/shared'
import { SIZE_ACTOR_NAME, SIZE_CATEGORY_NAME, SIZE_MOVIE_DESCRIPTION, SIZE_MOVIE_DIRECTOR, SIZE_MOVIE_TITLE } from '@src/modules/domain/constants'

export function checkProps (props: RequestToCreateMovie, formatDate = FormatDate()): CreateMovieProps {
  missingProps(props)

  emptyProps(props)

  actors(props)

  category(props)

  sizes(props)

  return {
    ...props,
    release: formatDate.toUS(props.release)
  }
}

function missingProps (props: Partial<RequestToCreateMovie>): void {
  if (
    !props.actors ||
    !props.category ||
    !props.description ||
    !props.director ||
    !props.release ||
    !props.title
  ) throw new Error('Missing required values')
}

function emptyProps (props: RequestToCreateMovie): void {
  if (
    !props.actors.length ||
    !props.description.trim() ||
    !props.director.trim() ||
    !props.release.trim() ||
    !props.title.trim()
  ) throw new Error('Missing required values')
}

function actors ({ actors }: RequestToCreateMovie): void {
  for (const actor of actors) {
    if (!actor.name || !actor.name.trim()) throw new Error('Missing required values')
  }
}

function category ({ category }: RequestToCreateMovie): void {
  if (!category.name || !category.name.trim()) throw new Error('Missing required values')
}

function sizes (props:RequestToCreateMovie): void {
  propsSizes(props.category.name, SIZE_CATEGORY_NAME)
  propsSizes(props.description, SIZE_MOVIE_DESCRIPTION)
  propsSizes(props.director, SIZE_MOVIE_DIRECTOR)
  propsSizes(props.title, SIZE_MOVIE_TITLE)

  for (const actor of props.actors) {
    propsSizes(actor.name, SIZE_ACTOR_NAME)
  }
}
