import { checkUpdateRating } from '../checkUpdateRating'

describe('Check Update Rating', () => {
  it('must be valide update rating props', () => {
    const props = checkUpdateRating({
      rating: 1
    })

    expect(props).toMatchObject({
      rating: 1
    })
  })

  it('throw new error if invalid number format', () => {
    expect(() => checkUpdateRating({ rating: 'one' as any })).toThrow()
  })

  it('throw new error if invalid rating', () => {
    expect(() => checkUpdateRating({ rating: -1 })).toThrow()
    expect(() => checkUpdateRating({ rating: 5 })).toThrow()
  })
})
