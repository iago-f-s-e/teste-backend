import { FormatDate } from '@domain/helpers'
import { SIZE_ACTOR_NAME, SIZE_CATEGORY_NAME, SIZE_MOVIE_DESCRIPTION, SIZE_MOVIE_DIRECTOR, SIZE_MOVIE_TITLE } from '@src/modules/domain/constants'
import { generateLongProps } from '@test/util/generateLongProps'
import { checkProps } from '../checkProps'

let mockFormatDate: typeof FormatDate

describe('Check Props', () => {
  beforeAll(() => {
    mockFormatDate = () => {
      return {
        toPT: jest.fn((payload: Date) => FormatDate().toPT(payload)),
        toUS: jest.fn((payload: string) => FormatDate().toUS(payload))
      }
    }
  })

  it('must be valide props request', () => {
    const mockedFormatDate = mockFormatDate()

    const props = checkProps({
      actors: [{
        name: 'Donald Glover'
      }],
      category: {
        name: 'Romance'
      },
      description: 'Something about movie',
      director: 'any director',
      release: '29/09/2021',
      title: 'any title'
    }, mockedFormatDate)

    expect(mockedFormatDate.toPT).not.toBeCalled()
    expect(mockedFormatDate.toUS).toBeCalledWith('29/09/2021')

    expect(props).toMatchObject({
      actors: [{
        name: 'Donald Glover'
      }],
      category: {
        name: 'Romance'
      },
      description: 'Something about movie',
      director: 'any director',
      title: 'any title'
    })

    expect(props.release).toBeInstanceOf(Date)
    expect(props.release.toISOString()).toBe('2021-09-29T03:00:00.000Z')
  })

  it('throw new error if missing required values', () => {
    const mockedFormatDate = mockFormatDate()

    const props: any = {}

    expect(() => checkProps(props, mockedFormatDate)).toThrow()

    expect(mockedFormatDate.toPT).not.toBeCalled()
    expect(mockedFormatDate.toUS).not.toBeCalledWith('29/09/2021')
  })

  it('throw new error if empty required values', () => {
    const mockedFormatDate = mockFormatDate()

    expect(() => checkProps({
      actors: [],
      category: {
        name: 'Romance'
      },
      description: ' ',
      director: ' ',
      release: ' ',
      title: ' '
    }, mockedFormatDate)).toThrow()

    expect(mockedFormatDate.toPT).not.toBeCalled()
    expect(mockedFormatDate.toUS).not.toBeCalledWith('29/09/2021')
  })

  it('throw new error if missing or empty actor name', () => {
    const mockedFormatDate = mockFormatDate()

    const props: any = {
      actors: [{ }],
      category: {
        name: 'Romance'
      },
      description: 'Something about movie',
      director: 'any director',
      release: '29/09/2021',
      title: 'any title'
    }

    expect(() => checkProps(props, mockedFormatDate)).toThrow()

    expect(() => checkProps({
      actors: [{
        name: ' '
      }],
      category: {
        name: 'Romance'
      },
      description: 'Something about movie',
      director: 'any director',
      release: '29/09/2021',
      title: 'any title'
    }, mockedFormatDate)).toThrow()

    expect(mockedFormatDate.toPT).not.toBeCalled()
    expect(mockedFormatDate.toUS).not.toBeCalledWith('29/09/2021')
  })

  it('throw new error if missing or empty category name', () => {
    const mockedFormatDate = mockFormatDate()

    const props: any = {
      actors: [{
        name: 'Donald Glover'
      }],
      category: {},
      description: 'Something about movie',
      director: 'any director',
      release: '29/09/2021',
      title: 'any title'
    }

    expect(() => checkProps(props, mockedFormatDate)).toThrow()

    expect(() => checkProps({
      actors: [{
        name: 'Donald Glover'
      }],
      category: {
        name: ' '
      },
      description: 'Something about movie',
      director: 'any director',
      release: '29/09/2021',
      title: 'any title'
    }, mockedFormatDate)).toThrow()

    expect(mockedFormatDate.toPT).not.toBeCalled()
    expect(mockedFormatDate.toUS).not.toBeCalledWith('29/09/2021')
  })

  it('throw new error if too long values', () => {
    const mockedFormatDate = mockFormatDate()

    expect(() => checkProps({
      actors: [{
        name: generateLongProps(SIZE_ACTOR_NAME + 1)
      }],
      category: {
        name: generateLongProps(SIZE_CATEGORY_NAME + 1)
      },
      description: generateLongProps(SIZE_MOVIE_DESCRIPTION + 1),
      director: generateLongProps(SIZE_MOVIE_DIRECTOR + 1),
      release: '29/09/2021',
      title: generateLongProps(SIZE_MOVIE_TITLE + 1)
    }, mockedFormatDate)).toThrow()

    expect(mockedFormatDate.toPT).not.toBeCalled()
    expect(mockedFormatDate.toUS).not.toBeCalledWith('29/09/2021')
  })
})
