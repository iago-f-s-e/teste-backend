export interface FormatDateProtocols {
  toUS: (date: string) => Date
  toPT: (date: Date) => string
}
