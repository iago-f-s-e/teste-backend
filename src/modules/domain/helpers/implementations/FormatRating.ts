export function FormatRating (rating: number, ratingAmount: number): number {
  return ratingAmount ? rating / ratingAmount : 0
}
