import { FormatDateProtocols } from '../contracts'

export function FormatDate (): FormatDateProtocols {
  return {
    toUS,
    toPT
  }
}

function toPT (usDate: Date): string {
  const usDateIsoString = usDate.toISOString().split('T')[0]

  return usDateIsoString.split('-').reverse().join('/')
}

function toUS (ptDate: string): Date {
  const usDate = ptDate.split('/').reverse().join('-')
  const usDateNumber = new Date(usDate).setHours(0, 0, 0, 0)
  const fixToLocaleDate = new Date(usDate).getDate() + 1

  return new Date(new Date(usDateNumber).setDate(fixToLocaleDate))
}
