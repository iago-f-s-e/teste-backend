import { FormatDate } from '../FormatDate'

describe('Format Date', () => {
  it('must convert pt date to us date', () => {
    const usDate = FormatDate().toUS('28/09/2021')

    expect(usDate.toISOString()).toBe('2021-09-28T03:00:00.000Z')
  })

  it('must convert us date to pt date', () => {
    const usDate = FormatDate().toUS('28/09/2021')

    const ptDate = FormatDate().toPT(usDate)

    expect(ptDate).toBe('28/09/2021')
  })
})
