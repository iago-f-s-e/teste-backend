import { TypeormHandles } from '@src/infra'
import { Movie } from '@domain/entities'

export interface MovieHandlers extends TypeormHandles<Movie> {}
