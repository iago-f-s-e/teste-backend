import { Movie } from '@domain/entities'
import { MappedMovie } from '@domain/controllers'

export interface MovieMappingProtocols {
  movie: (entity: Movie) => MappedMovie
  movies: (entities: Movie[]) => MappedMovie[]
}
