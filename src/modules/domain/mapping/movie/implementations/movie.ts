import { Movie } from '@domain/entities'
import { MappedMovie } from '@domain/controllers'
import { capitalize } from '@src/shared'
import { FormatDate, FormatRating } from '@src/modules/domain/helpers'
import { actorMapping } from '../../actor'
import { categoryMapping } from '../../category/implementations'

export function movie (entity: Movie): MappedMovie {
  return {
    id: entity.movieID,
    title: capitalize(entity.title),
    description: entity.description,
    director: capitalize(entity.director),
    rating: FormatRating(entity.rating, entity.ratingAmount),
    release: FormatDate().toPT(entity.release),
    category: categoryMapping().category(entity.category),
    actors: actorMapping().actors(entity.actors)
  }
}
