import { MovieMappingProtocols } from '../contracts'
import { movie } from './movie'
import { movies } from './movies'

export function movieMapping (): MovieMappingProtocols {
  return {
    movie,
    movies
  }
}
