import { MappedMovie } from '@domain/controllers'
import { Movie } from '@domain/entities'
import { movie } from './movie'

export function movies (entities: Movie[]): MappedMovie[] {
  return entities.length
    ? entities
      .map(entity => movie(entity))
      .sort((prev, next) => prev.title.localeCompare(next.title))
    : []
}
