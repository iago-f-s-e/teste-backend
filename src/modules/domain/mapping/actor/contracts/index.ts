import { Actor } from '@domain/entities'
import { MappedActor } from '@domain/controllers'

export interface ActorMappingProtocols {
  actor: (entity: Actor) => MappedActor
  actors: (entities: Actor[]) => MappedActor[]
}
