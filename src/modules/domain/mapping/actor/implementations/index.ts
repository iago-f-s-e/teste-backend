import { ActorMappingProtocols } from '../contracts'
import { actor } from './actor'
import { actors } from './actors'

export function actorMapping (): ActorMappingProtocols {
  return {
    actor,
    actors
  }
}
