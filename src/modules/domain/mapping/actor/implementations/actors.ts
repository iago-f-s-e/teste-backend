import { Actor } from '@domain/entities'
import { MappedActor } from '@domain/controllers'
import { actor } from './actor'

export function actors (entities: Actor[]): MappedActor[] {
  return entities.length
    ? entities
      .map(entity => actor(entity))
      .sort((prev, next) => prev.name.localeCompare(next.name))
    : []
}
