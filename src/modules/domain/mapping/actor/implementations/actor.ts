import { Actor } from '@domain/entities'
import { MappedActor } from '@domain/controllers'
import { capitalize } from '@src/shared'

export function actor ({ actorID, name }: Actor): MappedActor {
  return {
    id: actorID,
    name: capitalize(name)
  }
}
