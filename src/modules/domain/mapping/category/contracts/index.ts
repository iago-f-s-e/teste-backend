import { Category } from '@domain/entities'
import { MappedCategory } from '@domain/controllers'

export interface CategoryMappingProtocols {
  category: (entity: Category) => MappedCategory
}
