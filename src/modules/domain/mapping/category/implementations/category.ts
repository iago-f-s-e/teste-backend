import { Category } from '@domain/entities'
import { MappedCategory } from '@domain/controllers'
import { capitalize } from '@src/shared'

export function category ({ categoryID, name }: Category): MappedCategory {
  return {
    id: categoryID,
    name: capitalize(name)
  }
}
