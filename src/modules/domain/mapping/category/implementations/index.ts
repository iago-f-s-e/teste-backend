import { CategoryMappingProtocols } from '../contracts'
import { category } from './category'

export function categoryMapping (): CategoryMappingProtocols {
  return {
    category
  }
}
