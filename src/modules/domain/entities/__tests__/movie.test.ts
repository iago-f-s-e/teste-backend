import { validate } from 'uuid'
import { Movie } from '../Movie'

export interface AssignMovie extends Omit<Movie, 'movieID' | 'categoryID' | 'category' | 'actors'> {}

describe('Movie Entity', () => {
  it('must be possible to create a new movie', () => {
    const movie = new Movie()

    Object.assign<Movie, AssignMovie>(movie, {
      title: 'any title',
      director: 'any director',
      description: 'Something about movie',
      rating: 4,
      ratingAmount: 1,
      release: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
    })

    expect(movie).toMatchObject({
      title: 'any title',
      director: 'any director',
      description: 'Something about movie',
      rating: 4,
      ratingAmount: 1
    })
    expect(validate(movie.movieID)).toBe(true)
    expect(movie.createdAt).toBeInstanceOf(Date)
    expect(movie.updatedAt).toBeInstanceOf(Date)
    expect(movie.release).toBeInstanceOf(Date)
  })
})
