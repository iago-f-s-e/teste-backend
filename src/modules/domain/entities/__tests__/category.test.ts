import { validate } from 'uuid'
import { Category } from '../Category'

interface AssignCategory extends Pick<Category, 'name'> {}

describe('Category Entity', () => {
  it('must be possible to create a new category', () => {
    const category = new Category()

    Object.assign<Category, AssignCategory>(category, {
      name: 'romance'
    })

    expect(category).toMatchObject({
      name: 'romance'
    })

    expect(validate(category.categoryID)).toBe(true)
  })
})
