import { validate } from 'uuid'
import { Actor } from '../Actor'

interface AssignActor extends Pick<Actor, 'name'> {}

describe('Actor Entity', () => {
  it('must be possible to create a new actor', () => {
    const actor = new Actor()

    Object.assign<Actor, AssignActor>(actor, {
      name: 'Donald Glover'
    })

    expect(actor).toMatchObject({
      name: 'Donald Glover'
    })

    expect(validate(actor.actorID)).toBe(true)
  })
})
