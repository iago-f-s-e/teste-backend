import { UUID } from '@src/infra'
import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryColumn, UpdateDateColumn } from 'typeorm'
import { SIZE_MOVIE_TITLE, SIZE_MOVIE_DESCRIPTION, SIZE_MOVIE_DIRECTOR } from '../constants'
import { Actor } from './Actor'
import { Category } from './Category'

@Entity('movie')
export class Movie {
  @PrimaryColumn({ type: 'uuid', name: 'movie_id' })
  movieID!: string

  @Column({ type: 'uuid', name: 'category_id' })
  categoryID!: string

  @Column({ type: 'varchar', length: SIZE_MOVIE_TITLE })
  title!: string

  @Column({ type: 'varchar', length: SIZE_MOVIE_DESCRIPTION })
  description!: string

  @Column({ type: 'varchar', length: SIZE_MOVIE_DIRECTOR })
  director!: string

  @Column({ type: 'float', default: 0 })
  rating!: number

  @Column({ type: 'float', name: 'rating_amount', default: 0 })
  ratingAmount!: number

  @Column({ type: 'timestamp' })
  release!: Date

  @CreateDateColumn({ type: 'timestamp', name: 'created_at', default: 'now()' })
  createdAt!: Date

  @UpdateDateColumn({ type: 'timestamp', name: 'update_at', default: 'now()' })
  updatedAt!: Date

  @ManyToOne(() => Category, category => category.movies, {
    cascade: true,
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
  })
  @JoinColumn({ name: 'category_id', referencedColumnName: 'categoryID' })
  category!: Category

  @ManyToMany(() => Actor, actors => actors.movies, {
    cascade: true,
    eager: true,
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
  })
  @JoinTable()
  actors!: Actor[]

  constructor () {
    this.movieID = this.movieID || UUID().generate()
  }
}
