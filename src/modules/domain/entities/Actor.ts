import { UUID } from '@src/infra'
import { Column, Entity, ManyToMany, PrimaryColumn } from 'typeorm'
import { SIZE_ACTOR_NAME } from '../constants'
import { Movie } from './Movie'

@Entity('actor')
export class Actor {
  @PrimaryColumn({ type: 'uuid', name: 'actor_id' })
  actorID!: string

  @Column({ type: 'varchar', length: SIZE_ACTOR_NAME })
  name!: string

  @ManyToMany(() => Movie, movies => movies.actors)
  movies!: Movie

  constructor () {
    this.actorID = this.actorID || UUID().generate()
  }
}
