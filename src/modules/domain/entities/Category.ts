import { UUID } from '@src/infra'
import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm'
import { SIZE_CATEGORY_NAME } from '../constants'
import { Movie } from './Movie'

@Entity('category')
export class Category {
  @PrimaryColumn({ type: 'varchar', name: 'category_id' })
  categoryID!: string

  @Column({ type: 'varchar', length: SIZE_CATEGORY_NAME })
  name!: string

  @OneToMany(() => Movie, movies => movies.category)
  movies!: Movie[]

  constructor () {
    this.categoryID = this.categoryID || UUID().generate()
  }
}
