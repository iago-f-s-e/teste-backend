import { Errors, genericErrors } from '@src/shared'

export function movieErrors (message: string): Errors {
  if (message === 'Invalid rating') return { code: 400, message }

  if (message === 'Movie not found') return { code: 404, message }

  return genericErrors(message)
}
