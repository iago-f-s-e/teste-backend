export interface RequestToCreateMovie {
  title: string
  director: string
  description: string
  release: string
  category: { name: string }
  actors: { name: string }[]
}

export interface RequestToUpdateMovie {
  rating: number
}
