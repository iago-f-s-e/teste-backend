import { MappedActor, MappedCategory } from '@domain/controllers'

export interface MappedMovie {
  id: string
  title: string
  description: string
  director: string
  rating: number
  release: string
  category: MappedCategory
  actors: MappedActor[]
}
