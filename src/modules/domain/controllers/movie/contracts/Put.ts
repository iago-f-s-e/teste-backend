import { Request, Response } from 'express'

export interface MoviePutProtocols {
  requestToUpdateRating: (request: Request, response: Response) => Promise<Response>
}
