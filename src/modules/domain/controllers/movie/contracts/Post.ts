import { Request, Response } from 'express'

export interface MoviePostProtocols {
  requestToCreate: (request: Request, response: Response) => Promise<Response>
}
