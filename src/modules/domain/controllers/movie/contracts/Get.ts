import { Request, Response } from 'express'

export interface MovieGetProtocols {
  requestToList: (request: Request, response: Response) => Promise<Response>
}
