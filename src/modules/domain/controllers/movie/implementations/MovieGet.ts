import { Controller, Get, Middleware } from '@overnightjs/core'
import { Request, Response } from 'express'
import { MovieGetProtocols } from '../contracts'
import { movieMapping } from '@domain/mapping'
import { authUser } from '@src/modules/auth/middlewares'

@Controller('')
export class MovieGet implements MovieGetProtocols {
  @Get('')
  @Middleware(authUser)
  public async requestToList (request: Request, response: Response): Promise<Response> {
    const { instances } = request

    const movies = movieMapping()
      .movies(await instances.movie.find().execute(request.query))

    return response.status(200).json(movies)
  }
}
