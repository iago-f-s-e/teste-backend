import { Controller, Middleware, Put } from '@overnightjs/core'
import { Request, Response } from 'express'
import { MoviePutProtocols } from '../contracts'
import { UUID } from '@src/infra'
import { movieErrors } from '@domain/errors'
import { UpdateMovieProps } from '@domain/services'
import { updateRating } from '@domain/middlewares'
import { authUser } from '@src/modules/auth/middlewares'

@Controller('')
export class MoviePut implements MoviePutProtocols {
  @Put(':id')
  @Middleware([authUser, updateRating])
  public async requestToUpdateRating (request: Request, response: Response): Promise<Response> {
    try {
      const { instances } = request
      const { id: movieID } = request.params
      const { props }: {props: UpdateMovieProps} = request.body

      UUID().validate(movieID)

      const movie = await instances.movie.find().byID(movieID)

      await instances.movie.update().rating(props, movie)

      return response.status(200).json({ message: 'OK' })
    } catch (error: any) {
      const { code, message } = movieErrors(error.message)

      return response.status(code).json({ error: message })
    }
  }
}
