import { ChildControllers, Controller } from '@overnightjs/core'
import { MovieGet } from './MovieGet'
import { MoviePost } from './MoviePost'
import { MoviePut } from './MoviePut'

@Controller('movie')
@ChildControllers([
  new MoviePost(),
  new MovieGet(),
  new MoviePut()
])
export class MovieController {}
