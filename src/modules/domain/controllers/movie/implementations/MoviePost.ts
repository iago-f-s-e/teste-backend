import { Controller, Middleware, Post } from '@overnightjs/core'
import { Request, Response } from 'express'
import { MoviePostProtocols } from '../contracts'
import { CreateMovieProps } from '@domain/services'
import { createMovie } from '@domain/middlewares'
import { movieMapping } from '@domain/mapping'
import { authAdmin } from '@auth/middlewares'

@Controller('')
export class MoviePost implements MoviePostProtocols {
  @Post('')
  @Middleware([authAdmin, createMovie])
  public async requestToCreate (request: Request, response: Response): Promise<Response> {
    const { props }: {props: CreateMovieProps } = request.body
    const { instances } = request

    const movie = movieMapping().movie(await instances.movie.create().execute(props))

    return response.status(201).json(movie)
  }
}
