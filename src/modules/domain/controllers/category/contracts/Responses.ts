export interface MappedCategory {
  id: string
  name: string
}
