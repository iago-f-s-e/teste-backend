export interface MappedActor {
  id: string
  name: string
}
