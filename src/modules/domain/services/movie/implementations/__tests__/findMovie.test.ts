import { typeormHandlers } from '@src/infra'
import { Movie } from '@domain/entities'
import { Server } from '@src/server'
import { FindMovie } from '../FindMovie'
import { CreateMovie } from '../CreateMovie'

let findMovie: FindMovie
let server: Server
let movieCreated: Movie

describe('Find Movie', () => {
  beforeEach(async () => {
    server = new Server()
    await server.init()

    const movieHandlers = typeormHandlers(Movie)

    movieCreated = await new CreateMovie(movieHandlers).execute({
      actors: [{
        name: 'Donald Glover'
      }],
      category: {
        name: 'Romance'
      },
      description: 'Something about movie',
      director: 'any director',
      release: new Date(),
      title: 'any title'
    })

    findMovie = new FindMovie(movieHandlers)
  })

  afterEach(async () => {
    await server.closeConnection()
  })

  it('should return the movies found', async () => {
    const movies = await findMovie.execute()

    expect(movies).toEqual(
      expect.arrayContaining([
        movieCreated
      ])
    )
  })

  it('should return the movies found when searching by query with array entity', async () => {
    const movies = await findMovie.execute({
      actors: ['Donald', 'Glover'],
      category: 'Romance',
      title: 'title'
    })

    expect(movies).toEqual(
      expect.arrayContaining([
        movieCreated
      ])
    )
  })

  it('should return the movies found when searching by query with string entity', async () => {
    const movies = await findMovie.execute({
      actors: 'Glover' as any,
      category: 'Romance',
      title: 'title'
    })

    expect(movies).toEqual(
      expect.arrayContaining([
        movieCreated
      ])
    )
  })
})
