import { MovieHandlers } from '@domain/infra'
import { UUID } from '@src/infra'
import { Movie } from '@src/modules/domain/entities'
import { validate } from 'uuid'
import { CreateMovieProps } from '../../contracts'
import { MovieServices } from '../MovieServices'

let movieServices: MovieServices
let mockMovieHandles: MovieHandlers
const movies: Movie[] = []

const actorID = UUID().generate()
const categoryID = UUID().generate()
const movieID = UUID().generate()

const createMovieProps: CreateMovieProps = {
  actors: [{
    name: 'Donald Glover'
  }],
  category: {
    name: 'Romance'
  },
  description: 'Something about movie',
  director: 'any director',
  release: new Date(),
  title: 'any title'
}

describe('Movie Services', () => {
  beforeAll(() => {
    mockMovieHandles = {
      queryBuilder: {} as any,
      repository: {
        create: jest.fn((props: CreateMovieProps): Movie => ({
          description: props.description.toUpperCase(),
          director: props.director.toUpperCase(),
          title: props.title.toUpperCase(),
          release: props.release,
          actors: [{
            actorID,
            name: props.actors[0].name.toUpperCase(),
            movies: {} as any
          }],
          category: {
            categoryID,
            name: props.category.name.toUpperCase(),
            movies: {} as any
          },
          categoryID,
          movieID,
          rating: 0,
          ratingAmount: 0,
          createdAt: new Date(),
          updatedAt: new Date()
        })),
        save: jest.fn(async (movie: Movie) => {
          movies.push(movie)

          return Promise.resolve(movie).then(response => response)
        })
      } as any
    }

    movieServices = new MovieServices(mockMovieHandles)
  })

  describe('Create Movie', () => {
    it('should be possible to create a new movie', async () => {
      const movie = await movieServices.create().execute(createMovieProps)

      expect(mockMovieHandles.repository.create).toBeCalledWith(createMovieProps)
      expect(mockMovieHandles.repository.save).toBeCalledWith(movie)

      expect(validate(movie.movieID)).toBe(true)
      expect(validate(movie.categoryID)).toBe(true)
      expect(validate(movie.category.categoryID)).toBe(true)
      expect(validate(movie.actors[0].actorID)).toBe(true)

      expect(movie.release).toBeInstanceOf(Date)
      expect(movie.createdAt).toBeInstanceOf(Date)
      expect(movie.updatedAt).toBeInstanceOf(Date)
    })
  })
})
