import { MovieHandlers } from '@domain/infra'
import { Movie } from '@domain/entities'
import { CreateMovieProps } from '../../contracts'
import { CreateMovie } from '../CreateMovie'
import { UUID } from '@src/infra'
import { validate } from 'uuid'

let createMovie: CreateMovie
let mockMovieHandles: MovieHandlers

const createMovieProps: CreateMovieProps = {
  actors: [{
    name: 'Donald Glover'
  }],
  category: {
    name: 'Romance'
  },
  description: 'Something about movie',
  director: 'any director',
  release: new Date(),
  title: 'any title'
}

describe('Create Movie', () => {
  beforeAll(() => {
    mockMovieHandles = {
      queryBuilder: {} as any,
      repository: {
        create: jest.fn((props: CreateMovieProps): Movie => ({
          ...props,
          actors: [{
            actorID: UUID().generate(),
            name: props.actors[0].name,
            movies: {} as any
          }],
          category: {
            categoryID: UUID().generate(),
            name: props.category.name,
            movies: {} as any
          },
          categoryID: UUID().generate(),
          movieID: UUID().generate(),
          rating: 0,
          ratingAmount: 0,
          createdAt: new Date(),
          updatedAt: new Date()
        })),
        save: jest.fn((movie: Movie) => Promise.resolve(movie).then(response => response))
      } as any
    }

    createMovie = new CreateMovie(mockMovieHandles)
  })

  it('should be possible to create a new movie', async () => {
    const movie = await createMovie.execute(createMovieProps)

    expect(mockMovieHandles.repository.create).toBeCalledWith(createMovieProps)
    expect(mockMovieHandles.repository.save).toBeCalledWith(movie)

    expect(movie).toMatchObject(createMovieProps)

    expect(validate(movie.movieID)).toBe(true)
    expect(validate(movie.categoryID)).toBe(true)
    expect(validate(movie.category.categoryID)).toBe(true)
    expect(validate(movie.actors[0].actorID)).toBe(true)

    expect(movie.release).toBeInstanceOf(Date)
    expect(movie.createdAt).toBeInstanceOf(Date)
    expect(movie.updatedAt).toBeInstanceOf(Date)
  })
})
