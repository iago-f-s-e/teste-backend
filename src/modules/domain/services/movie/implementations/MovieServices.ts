import { MovieServicesProtocols } from '../contracts'
import { MovieHandlers } from '@domain/infra'
import { FindMovie, CreateMovie, UpdateMovie } from '.'

export class MovieServices implements MovieServicesProtocols {
  constructor (private readonly movieHandlers: MovieHandlers) {}

  public create (): CreateMovie {
    return new CreateMovie(this.movieHandlers)
  }

  public find (): FindMovie {
    return new FindMovie(this.movieHandlers)
  }

  public update (): UpdateMovie {
    return new UpdateMovie(this.movieHandlers)
  }
}
