import { MovieHandlers } from '@domain/infra'
import { Movie } from '@domain/entities'
import { FindMovieProtocols, Filters } from '../contracts'

export class FindMovie implements FindMovieProtocols {
  constructor (private readonly movieHandlers: MovieHandlers) {}

  private getNames (actors: string[] | undefined): string[] {
    const isEmpty = (!actors || !actors.length)

    if (isEmpty) return ['%%']

    if (Array.isArray(actors)) return actors.map(name => `%${name}%`)

    return [`%${actors}%`]
  }

  public async byID (movieID: string): Promise<Movie> {
    const movie = await this.movieHandlers.repository.findOne({ where: { movieID } })

    if (!movie) throw new Error('Movie not found')

    return movie
  }

  public async execute (filters?: Filters): Promise<Movie[]> {
    const names = this.getNames(filters?.actors)

    return this.movieHandlers.queryBuilder
      .innerJoinAndSelect('Movie.category', 'category')
      .innerJoinAndSelect('Movie.actors', 'actors')
      .where('Movie.title ILIKE :title', { title: `%${filters?.title || ''}%` })
      .andWhere('category.name ILIKE :name', { name: `%${filters?.category || ''}%` })
      .andWhere('actors.name ILIKE ANY (:names)', { names })
      .getMany()
  }
}
