import { MovieHandlers } from '@domain/infra'
import { Movie } from '@domain/entities'
import { UpdateMovieProtocols, UpdateMovieProps } from '../contracts'

export class UpdateMovie implements UpdateMovieProtocols {
  constructor (private readonly movieHandlers: MovieHandlers) {}

  public async rating (
    { rating: updateRating }: UpdateMovieProps,
    { rating: originalRating, ratingAmount: originalRatingAmount, movieID }: Movie
  ): Promise<void> {
    const rating = updateRating ? originalRating + updateRating : originalRating
    const ratingAmount = originalRating ? originalRatingAmount + 1 : originalRatingAmount

    await this.movieHandlers.repository.update({ movieID }, { rating, ratingAmount })
  }
}
