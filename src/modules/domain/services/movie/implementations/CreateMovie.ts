import { Movie } from '@domain/entities'
import { MovieHandlers } from '@domain/infra'
import { CreateMovieProps, CreateMovieProtocols } from '../contracts'

export class CreateMovie implements CreateMovieProtocols {
  constructor (private readonly movieHandlers: MovieHandlers) {}

  private create (props: CreateMovieProps): Movie {
    return this.movieHandlers.repository.create(props)
  }

  private save (movie: Movie): Promise<Movie> {
    return this.movieHandlers.repository.save(movie)
  }

  public async execute (props: CreateMovieProps): Promise<Movie> {
    return this.save(this.create(props))
  }
}
