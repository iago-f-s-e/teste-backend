import { Movie } from '@domain/entities'

export interface Filters {
  title?: string
  category?: string
  actors?: string[]
}

export interface FindMovieProtocols {
  byID: (movieID: string) => Promise<Movie>
  execute: (filters?: Filters) => Promise<Movie[]>
}
