import { CreateMovie, FindMovie, UpdateMovie } from '../implementations'

export interface MovieServicesProtocols {
  create: () => CreateMovie
  find: () => FindMovie
  update: () => UpdateMovie
}
