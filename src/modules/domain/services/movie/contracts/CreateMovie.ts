import { RequestToCreateMovie } from '@domain/controllers'
import { Movie } from '@domain/entities'

export interface CreateMovieProps extends Omit<RequestToCreateMovie, 'release'> {
  release: Date
}

export interface CreateMovieProtocols {
  execute: (props: CreateMovieProps) => Promise<Movie>
}
