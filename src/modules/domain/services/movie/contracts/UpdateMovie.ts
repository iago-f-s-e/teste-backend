import { RequestToUpdateMovie } from '@domain/controllers'
import { Movie } from '@domain/entities'

export interface UpdateMovieProps extends Partial<RequestToUpdateMovie> {}

export interface UpdateMovieProtocols {
  rating: (updateProps: UpdateMovieProps, originalProps: Movie) => Promise<void>
}
