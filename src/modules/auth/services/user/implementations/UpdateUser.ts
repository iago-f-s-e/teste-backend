import { UserHandlers } from '@auth/infra'
import { User } from '@auth/entities'
import { UpdateUserProps, UpdateUserProtocols } from '..'

export class UpdateUser implements UpdateUserProtocols {
  constructor (private readonly userHandlers: UserHandlers) {}

  public async inactivate (userID: string): Promise<void> {
    await this.userHandlers.repository.update({ userID }, {
      isActive: false
    })
  }

  public async admin (userID: string): Promise<void> {
    await this.userHandlers.repository.update({ userID }, {
      isAdmin: true
    })
  }

  public async execute (
    { name: updateName, username: updateUsername }: UpdateUserProps,
    { name: originalName, username: originalUsername, userID }: User): Promise<void> {
    await this.userHandlers.repository.update({ userID }, {
      name: updateName || updateUsername,
      username: originalName || originalUsername
    })
  }
}
