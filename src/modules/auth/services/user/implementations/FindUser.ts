import { UserHandlers } from '@auth/infra'
import { User } from '@auth/entities'
import { FindUserProtocols } from '../contracts'

export class FindUser implements FindUserProtocols {
  constructor (private readonly userHandlers: UserHandlers) {}

  public async byCreationEmail (email: string): Promise<User | undefined> {
    return this.userHandlers.repository.findOne({
      where: { email }
    })
  }

  public async byCreationUsername (username: string): Promise<User | undefined> {
    return this.userHandlers.repository.findOne({
      where: { username }
    })
  }

  public async byEmail (email: string): Promise<User> {
    const user = await this.userHandlers.queryBuilder
      .addSelect('User.password')
      .where('User.email = :email', { email })
      .getOne()

    if (!user) throw new Error('Invalid email')

    if (!user.isActive) throw new Error('Deleted user')

    return user
  }

  public async byID (userID: string): Promise<User> {
    const user = await this.userHandlers.repository.findOne({
      where: { userID }
    })

    if (!user) throw new Error('User not found')

    if (!user.isActive) throw new Error('Deleted user')

    return user
  }
}
