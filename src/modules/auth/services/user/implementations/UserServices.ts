import { UserHandlers } from '@auth/infra'
import { UserServicesProtocols } from '../contracts'
import { CreateUser, FindUser, UpdateUser } from '.'

export class UserServices implements UserServicesProtocols {
  constructor (private readonly userHandlers: UserHandlers) {}

  public create (): CreateUser {
    return new CreateUser(this.userHandlers)
  }

  public find (): FindUser {
    return new FindUser(this.userHandlers)
  }

  public update (): UpdateUser {
    return new UpdateUser(this.userHandlers)
  }
}
