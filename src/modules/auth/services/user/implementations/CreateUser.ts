import bcrypt from 'bcrypt'
import { UserHandlers } from '@auth/infra'
import { User } from '@auth/entities'
import { CreateUserProps, CreateUserProtocols } from '../contracts'

export class CreateUser implements CreateUserProtocols {
  constructor (private readonly userHandlers: UserHandlers) {}

  private async hashPassword (password: string, salt = 10): Promise<string> {
    return await bcrypt.hash(password, salt)
  }

  private async create (props:CreateUserProps): Promise<User> {
    props.password = await this.hashPassword(props.password)

    return this.userHandlers.repository.create(props)
  }

  private async save (user: User): Promise<User> {
    return this.userHandlers.repository.save(user)
  }

  public async execute (props: CreateUserProps): Promise<User> {
    return this.save(await this.create(props))
  }
}
