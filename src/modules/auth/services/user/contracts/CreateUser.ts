import { RequestToCreateUser } from '@auth/controllers'
import { User } from '@auth/entities'

export interface CreateUserProps extends RequestToCreateUser {}

export interface CreateUserProtocols {
  execute: (props: CreateUserProps) => Promise<User>
}
