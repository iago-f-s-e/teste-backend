import { CreateUser, FindUser, UpdateUser } from '../implementations'

export interface UserServicesProtocols {
  create: () => CreateUser
  find: () => FindUser
  update: () => UpdateUser
}
