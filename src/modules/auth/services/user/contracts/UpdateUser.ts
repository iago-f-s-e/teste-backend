import { RequestToUpdate } from '@auth/controllers'
import { User } from '@auth/entities'

export interface UpdateUserProps extends RequestToUpdate {}

export interface UpdateUserProtocols {
  inactivate: (userID: string) => Promise<void>
  admin: (userID: string) => Promise<void>
  execute: (updateProps: UpdateUserProps, originalProps: User) => Promise<void>
}
