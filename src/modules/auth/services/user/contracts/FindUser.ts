import { User } from '@auth/entities'

export interface FindUserProtocols {
  byCreationEmail: (email: string) => Promise<User | undefined>
  byCreationUsername: (username: string) => Promise<User | undefined>
  byEmail: (email: string) => Promise<User>
  byID: (userID: string) => Promise<User>
}
