import { UUID } from '@src/infra'
import { Column, Entity, PrimaryColumn } from 'typeorm'
import { SIZE_USER_EMAIL, SIZE_USER_NAME, SIZE_USER_USERNAME } from '../constants'

@Entity('user')
export class User {
  @PrimaryColumn({ type: 'uuid', name: 'user_id' })
  userID!: string

  @Column({ type: 'varchar', length: SIZE_USER_NAME })
  name!: string

  @Column({ type: 'varchar', length: SIZE_USER_USERNAME })
  username!: string

  @Column({ type: 'varchar', length: SIZE_USER_EMAIL })
  email!: string

  @Column({ type: 'varchar', select: false })
  password!: string

  @Column({ type: 'boolean', name: 'is_active', default: true })
  isActive!: boolean

  @Column({ type: 'boolean', name: 'is_admin', default: false })
  isAdmin!: boolean

  constructor () {
    this.userID = UUID().generate()
  }
}
