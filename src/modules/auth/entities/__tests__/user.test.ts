import { validate } from 'uuid'
import { User } from '../User'

interface AssignUser extends Omit<User, 'userID' | 'password'> {}

describe('User Entity', () => {
  it('must be possible to create a new actor', () => {
    const user = new User()

    Object.assign<User, AssignUser>(user, {
      name: 'John Doe',
      username: 'john_doe',
      email: 'johndoe@gmail.com',
      isActive: true,
      isAdmin: false
    })

    expect(user).toMatchObject({
      name: 'John Doe',
      username: 'john_doe',
      email: 'johndoe@gmail.com',
      isActive: true,
      isAdmin: false
    })

    expect(validate(user.userID)).toBe(true)
    expect(user.password).toBeUndefined()
  })
})
