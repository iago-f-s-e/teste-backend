import { Errors, genericErrors } from '@src/shared'

export function userErrors (message: string): Errors {
  console.log(message)

  if (
    message === 'Email already exists' ||
    message === 'Username already exists' ||
    message === 'Deleted user' ||
    message === 'Unauthorized'
  ) return { code: 401, message }

  if (
    message === 'Invalid email' ||
    message === 'Invalid password' ||
    message === 'Invalid token' ||
    message === 'Invalid authentication' ||
    message === 'jwt malformed' ||
    message === 'jwt expired' ||
    message === 'invalid signature'
  ) return { code: 401, message: 'Invalid data' }

  if (message === 'User not found') return { code: 404, message }

  return genericErrors(message)
}
