import { User } from '@auth/entities'
import { MappedUser, MappedUserWithToken } from '@auth/controllers'

export interface UserMappingProtocols {
  user: (entity: User) => MappedUser
  userWithToken: (entity: User) => MappedUserWithToken
}
