import { User } from '@auth/entities'
import { MappedUser } from '@auth/controllers'

export function user ({ email, isAdmin, name, userID, username }: User): MappedUser {
  return {
    id: userID,
    name,
    email,
    username,
    admin: isAdmin || undefined
  }
}
