import { UserMappingProtocols } from '../contracts'
import { user, userWithToken } from '.'

export function userMapping (): UserMappingProtocols {
  return {
    user,
    userWithToken
  }
}
