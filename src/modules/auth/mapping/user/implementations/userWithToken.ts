import jwt from 'jsonwebtoken'
import { User } from '@auth/entities'
import { user as mapUser } from './user'
import { MappedUserWithToken } from '@src/modules/auth/controllers'
import { PayloadToken } from '../contracts/PayloadToken'

export function userWithToken (entity: User): MappedUserWithToken {
  const user = mapUser(entity)

  return {
    user,
    token: generateToken({ id: user.id })
  }
}

function generateToken (payloadToken: PayloadToken): string {
  const payload = JSON.parse(JSON.stringify(payloadToken))

  return jwt.sign(payload, process.env.AUTH_KEY_SECURITY as string, {
    expiresIn: process.env.AUTH_KEY_TOKEN_EXPIRES
  })
}
