import { TypeormHandles } from '@src/infra'
import { User } from '@auth/entities'

export interface UserHandlers extends TypeormHandles<User> {}
