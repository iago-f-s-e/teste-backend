import { PayloadToken } from '@auth/mapping/user/contracts/PayloadToken'
import jwt from 'jsonwebtoken'

export function checkToken (payload: string | undefined): PayloadToken {
  if (!payload || !payload.trim()) throw new Error('Invalid token')

  const [bearer, token] = payload.split(' ')

  if (bearer !== 'Bearer') throw new Error('Invalid authentication')

  return jwt.verify(token, process.env.AUTH_KEY_SECURITY as string) as PayloadToken
}
