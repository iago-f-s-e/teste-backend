import { UpdateUserProps } from '@auth/services'
import { RequestToUpdate } from '@auth/controllers'
import { propsSizes } from '@src/shared'
import { SIZE_USER_NAME, SIZE_USER_USERNAME } from '@auth/constants'

export function checkUpdateProps (props: RequestToUpdate): UpdateUserProps {
  sizes(props)

  return props
}

function sizes (props: RequestToUpdate): void {
  if (props.name) propsSizes(props.name, SIZE_USER_NAME)
  if (props.username) propsSizes(props.username, SIZE_USER_USERNAME)
}
