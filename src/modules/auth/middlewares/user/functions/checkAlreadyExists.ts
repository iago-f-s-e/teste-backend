import { CreateUserProps, UserServices } from '@auth/services'

export async function checkAlreadyExists (
  { email, username }: CreateUserProps,
  services: UserServices
): Promise<void> {
  const emailExists = await services.find().byCreationEmail(email)

  if (emailExists) throw new Error('Email already exists')

  const usernameExists = await services.find().byCreationUsername(username)

  if (usernameExists) throw new Error('Username already exists')
}
