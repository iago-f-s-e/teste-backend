import { User } from '@auth/entities'
import { UserServices } from '@auth/services'

export async function checkUser (id: string, services: UserServices): Promise<User> {
  return await services.find().byID(id)
}
