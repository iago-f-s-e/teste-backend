import { CreateUserProps } from '@auth/services'
import { RequestToCreateUser } from '@auth/controllers'
import { propsSizes } from '@src/shared'
import { SIZE_USER_EMAIL, SIZE_USER_NAME, SIZE_USER_USERNAME } from '@auth/constants'

export function checkProps (props: RequestToCreateUser): CreateUserProps {
  missingProps(props)

  emptyProps(props)

  sizes(props)

  return props
}

function missingProps (props: Partial<RequestToCreateUser>): void {
  if (
    !props.email ||
    !props.name ||
    !props.password ||
    !props.username
  ) throw new Error('Missing required values')
}

function emptyProps (props: RequestToCreateUser): void {
  if (
    !props.email.trim() ||
    !props.name.trim() ||
    !props.password.trim() ||
    !props.username.trim()
  ) throw new Error('Missing required values')
}

function sizes (props:RequestToCreateUser): void {
  propsSizes(props.email, SIZE_USER_EMAIL)
  propsSizes(props.name, SIZE_USER_NAME)
  propsSizes(props.username, SIZE_USER_USERNAME)
}
