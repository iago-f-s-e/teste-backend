import { userErrors } from '@auth/errors'
import { NextFunction, Request, Response } from 'express'

import { checkAlreadyExists, checkProps } from '../functions'

export async function createUser (request: Request, response: Response, next: NextFunction): Promise<void> {
  try {
    const { instances } = request

    const props = checkProps(request.body)

    await checkAlreadyExists(props, instances.user)

    request.body = { props }

    next()
  } catch (error: any) {
    const { code, message } = userErrors(error.message)

    response.status(code).json({ error: message })
  }
}
