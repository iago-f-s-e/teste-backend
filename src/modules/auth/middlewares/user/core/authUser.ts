import { userErrors } from '@auth/errors'
import { UUID } from '@src/infra'
import { NextFunction, Request, Response } from 'express'

import { checkToken, checkUser } from '../functions'

export async function authUser (request: Request, response: Response, next: NextFunction): Promise<void> {
  try {
    const { instances } = request

    const { id } = checkToken(request.headers.authorization)

    UUID().validate(id)

    request.user = await checkUser(id, instances.user)

    next()
  } catch (error: any) {
    const { code, message } = userErrors(error.message)

    response.status(code).json({ error: message })
  }
}
