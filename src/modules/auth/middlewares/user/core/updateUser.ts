import { userErrors } from '@auth/errors'
import { NextFunction, Request, Response } from 'express'

import { checkUpdateProps } from '../functions'

export async function createUser (request: Request, response: Response, next: NextFunction): Promise<void> {
  try {
    const props = checkUpdateProps(request.body)

    request.body = { props }

    next()
  } catch (error: any) {
    const { code, message } = userErrors(error.message)

    response.status(code).json({ error: message })
  }
}
