export interface MappedUser {
  id: string
  name: string
  email: string
  username: string
  admin?: boolean
}

export interface MappedUserWithToken {
  user: MappedUser
  token: string
}
