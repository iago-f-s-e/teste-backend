import { Request, Response } from 'express'

export interface UserPostProtocols {
  requestToCreate: (request: Request, response: Response) => Promise<Response>
  requestToLogin: (request: Request, response: Response) => Promise<Response>
}
