import { Request, Response } from 'express'

export interface UserDeleteProtocols {
  requestToDelete: (request: Request, response: Response) => Promise<Response>
}
