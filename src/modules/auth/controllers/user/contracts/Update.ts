import { Request, Response } from 'express'

export interface UserUpdateProtocols {
  requestToAdmin: (request: Request, response: Response) => Promise<Response>
  requestToUpdate: (request: Request, response: Response) => Promise<Response>
}
