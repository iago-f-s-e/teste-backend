export interface RequestToCreateUser {
  name: string
  username: string
  email: string
  password: string
}

export interface RequestToLogin {
  email: string
  password: string
}

export interface RequestToUpdate {
  name?: string
  username?: string
}
