import { Controller, Delete, Middleware } from '@overnightjs/core'
import { Request, Response } from 'express'
import { UserDeleteProtocols } from '../contracts'
import { authUser } from '@auth/middlewares'
import { userErrors } from '@auth/errors'
import { UUID } from '@src/infra'

@Controller('')
export class UserDelete implements UserDeleteProtocols {
  @Delete(':id')
  @Middleware(authUser)
  public async requestToDelete (request: Request, response: Response): Promise<Response> {
    const { id } = request.params
    const { user, instances } = request

    try {
      UUID().validate(id)

      if (id !== user.userID) throw new Error('Unauthorized')

      await instances.user.update().inactivate(id)

      return response.status(200).json({ message: 'OK' })
    } catch (error: any) {
      const { code, message } = userErrors(error.message)

      return response.status(code).json({ error: message })
    }
  }
}
