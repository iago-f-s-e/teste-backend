import { ChildControllers, Controller } from '@overnightjs/core'
import { UserDelete } from './UserDelete'
import { UserPost } from './UserPost'
import { UserUpdate } from './UserUpdate'

@Controller('user')
@ChildControllers([
  new UserPost(),
  new UserDelete(),
  new UserUpdate()
])
export class UserController {}
