import { Controller, Middleware, Post } from '@overnightjs/core'
import { Request, Response } from 'express'
import { UserPostProtocols, RequestToLogin } from '../contracts'
import { createUser } from '@auth/middlewares'
import { CreateUserProps } from '@auth/services'
import { userMapping } from '@auth/mapping'
import { comparePassword } from '@auth/utils'
import { userErrors } from '@auth/errors'

@Controller('')
export class UserPost implements UserPostProtocols {
  @Post('')
  @Middleware(createUser)
  public async requestToCreate (request: Request, response: Response): Promise<Response> {
    const { props }: {props: CreateUserProps } = request.body
    const { instances } = request

    const userWithToken = userMapping().userWithToken(await instances.user.create().execute(props))

    return response.status(201).json(userWithToken)
  }

  @Post('login')
  public async requestToLogin (request: Request, response: Response): Promise<Response> {
    const props: RequestToLogin = request.body
    const { instances } = request

    try {
      const user = await instances.user.find().byEmail(props.email)

      await comparePassword(props.password, user.password)

      const userWithToken = userMapping().userWithToken(user)

      return response.status(201).json(userWithToken)
    } catch (error: any) {
      const { code, message } = userErrors(error.message)

      return response.status(code).json({ error: message })
    }
  }
}
