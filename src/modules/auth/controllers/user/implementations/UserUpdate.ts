import { ClassMiddleware, Controller, Patch } from '@overnightjs/core'
import { Request, Response } from 'express'
import { UserUpdateProtocols } from '../contracts'
import { authUser } from '@auth/middlewares'
import { userErrors } from '@auth/errors'
import { UUID } from '@src/infra'
import { UpdateUserProps } from '@auth/services'

@Controller('')
@ClassMiddleware(authUser)
export class UserUpdate implements UserUpdateProtocols {
  @Patch(':id')
  public async requestToAdmin (request: Request, response: Response): Promise<Response> {
    const { id } = request.params
    const { user, instances } = request

    try {
      UUID().validate(id)

      if (id !== user.userID) throw new Error('Unauthorized')

      await instances.user.update().admin(id)

      return response.status(200).json({ message: 'OK' })
    } catch (error: any) {
      const { code, message } = userErrors(error.message)

      return response.status(code).json({ error: message })
    }
  }

  public async requestToUpdate (request: Request, response: Response): Promise<Response> {
    const { id } = request.params
    const { user, instances } = request
    const { props }: {props: UpdateUserProps} = request.body

    try {
      UUID().validate(id)

      if (id !== user.userID) throw new Error('Unauthorized')

      await instances.user.update().execute(props, user)

      return response.status(200).json({ message: 'OK' })
    } catch (error: any) {
      const { code, message } = userErrors(error.message)

      return response.status(code).json({ error: message })
    }
  }
}
