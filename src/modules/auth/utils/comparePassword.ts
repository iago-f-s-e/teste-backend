import bcrypt from 'bcrypt'

export async function comparePassword (password: string, hashedPassword: string): Promise<void> {
  const isValid = await bcrypt.compare(password, hashedPassword)

  if (!isValid) throw new Error('Invalid password')
}
