export function generateLongProps (size: number): string {
  let prop = ''

  while (prop.length <= size) {
    prop += '@'
  }

  return prop
}
