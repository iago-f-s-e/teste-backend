import { SIZE_ACTOR_NAME, SIZE_CATEGORY_NAME, SIZE_MOVIE_DESCRIPTION, SIZE_MOVIE_DIRECTOR, SIZE_MOVIE_TITLE } from '@src/modules/domain/constants'
import { generateLongProps } from '@test/util/generateLongProps'

describe('Movie Post', () => {
  it('should return status code 201 and the movie when creating successfully', async () => {
    const { status, body } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [{
          name: 'Donald Glover'
        }],
        category: {
          name: 'Romance'
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      })

    expect(status).toBe(201)
    expect(body).toMatchObject({
      title: 'any title',
      description: 'Something about movie',
      director: 'any director',
      rating: 0,
      release: '29/09/2021',
      category: { name: 'Romance' },
      actors: [
        {
          name: 'Donald Glover'
        }
      ]
    })
  })

  it('should return status code 400 if missing required values', async () => {
    const { body } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({})
      .expect(400)

    expect(body.error).toBeTruthy()
  })

  it('should return status code 400 if empty required values', async () => {
    const { body } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [],
        category: {
          name: 'Romance'
        },
        description: ' ',
        director: ' ',
        release: ' ',
        title: ' '
      })
      .expect(400)

    expect(body.error).toBeTruthy()
  })

  it('should return status code 400 if missing or empty actor name', async () => {
    const { body: missing } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [{ }],
        category: {
          name: 'Romance'
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      })
      .expect(400)

    const { body: empty } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [{
          name: ' '
        }],
        category: {
          name: 'Romance'
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      })
      .expect(400)

    expect(empty.error).toBeTruthy()
    expect(missing.error).toBeTruthy()
  })

  it('should return status code 400 if missing or empty category name', async () => {
    const { body: missing } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [{
          name: 'Donald Glover'
        }],
        category: { },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      })
      .expect(400)

    const { body: empty } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [{
          name: 'Donald Glover'
        }],
        category: {
          name: ' '
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      })
      .expect(400)

    expect(empty.error).toBeTruthy()
    expect(missing.error).toBeTruthy()
  })

  it('should return status code 400 if value too long', async () => {
    const { body } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [{
          name: generateLongProps(SIZE_ACTOR_NAME + 1)
        }],
        category: {
          name: generateLongProps(SIZE_CATEGORY_NAME + 1)
        },
        description: generateLongProps(SIZE_MOVIE_DESCRIPTION + 1),
        director: generateLongProps(SIZE_MOVIE_DIRECTOR + 1),
        release: '29/09/2022',
        title: generateLongProps(SIZE_MOVIE_TITLE + 1)
      })
      .expect(400)

    expect(body.error).toBeTruthy()
  })
})
