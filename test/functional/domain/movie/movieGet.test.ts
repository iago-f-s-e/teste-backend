import { MappedMovie } from '@domain/controllers'

let movieCreated: MappedMovie

describe('Movie Get', () => {
  beforeAll(async () => {
    const { body } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [{
          name: 'Donald Glover'
        }],
        category: {
          name: 'Romance'
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      })

    movieCreated = body
  })

  it('should return status code 200 and movies found', async () => {
    const { body: moviesWithoutQuery } = await global.testRequest
      .get(`/api/${process.env.API_VERSION}/movie`)
      .expect(200)

    const { body: movieWithQueryString } = await global.testRequest
      .get(`/api/${process.env.API_VERSION}/movie`)
      .query({
        actors: 'Glover',
        title: 'title',
        category: 'Romance'
      })
      .expect(200)

    const { body: movieWithQueryArray } = await global.testRequest
      .get(`/api/${process.env.API_VERSION}/movie`)
      .query({
        actors: ['Glover', 'Donald'],
        title: 'title',
        category: 'Romance'
      })
      .expect(200)

    const { body: movieWithQueryEmpty } = await global.testRequest
      .get(`/api/${process.env.API_VERSION}/movie`)
      .query({
        title: 'title',
        category: 'Romance'
      })
      .expect(200)

    expect(moviesWithoutQuery).toEqual(expect.arrayContaining([movieCreated]))
    expect(movieWithQueryEmpty).toEqual(expect.arrayContaining([movieCreated]))
    expect(movieWithQueryArray).toEqual(expect.arrayContaining([movieCreated]))
    expect(movieWithQueryString).toEqual(expect.arrayContaining([movieCreated]))
  })
})
