import { MappedMovie } from '@domain/controllers'
import { UUID } from '@src/infra'

let movieCreated: MappedMovie

describe('Movie Put', () => {
  beforeAll(async () => {
    const { body } = await global.testRequest
      .post(`/api/${process.env.API_VERSION}/movie`)
      .send({
        actors: [{
          name: 'Donald Glover'
        }],
        category: {
          name: 'Romance'
        },
        description: 'Something about movie',
        director: 'any director',
        release: '29/09/2021',
        title: 'any title'
      })

    movieCreated = body
  })

  it('should return status code 200 when update successfully', async () => {
    const { body } = await global.testRequest
      .put(`/api/${process.env.API_VERSION}/movie/${movieCreated.id}`)
      .send({ rating: 1 })
      .expect(200)

    expect(body).toMatchObject({ message: 'OK' })
  })

  it('should return status code 400 if invalid rating format', async () => {
    const { body } = await global.testRequest
      .put(`/api/${process.env.API_VERSION}/movie/${movieCreated.id}`)
      .send({ rating: 'one' })
      .expect(400)

    expect(body.error).toBeTruthy()
  })

  it('should return status code 400 if invalid rating', async () => {
    const { body } = await global.testRequest
      .put(`/api/${process.env.API_VERSION}/movie/${movieCreated.id}`)
      .send({ rating: -1 })
      .expect(400)

    expect(body.error).toBeTruthy()
  })

  it('should return status code 404 if movie not found', async () => {
    const { body } = await global.testRequest
      .put(`/api/${process.env.API_VERSION}/movie/${UUID().generate()}`)
      .send({ rating: 3 })
      .expect(404)

    expect(body.error).toBeTruthy()
  })

  it('should return status code 400 if invalid params id', async () => {
    const { body } = await global.testRequest
      .put(`/api/${process.env.API_VERSION}/movie/movieID`)
      .send({ rating: 3 })
      .expect(400)

    expect(body.error).toBeTruthy()
  })
})
